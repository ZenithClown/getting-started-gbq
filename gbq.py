# -*- encoding: utf-8 -*-

"""Helper Function for Google Big-Query"""

__author__       = "Debmalya Pramanik"
__author_email__ = "dPramanik.official@gmail.com"

__credits__      = None

__status__       = "Development"
__version__      = "0.0.1"
__docformat__    = "camelCasing"

__copyright__    = "Copyright (c) 2021 Debmalya Pramanik"

import pandas as pd
from google.cloud import bigquery

def _write_preference(mode : str) -> str:
    # defines write preference for a table
    # https://cloud.google.com/bigquery/docs/loading-data-cloud-storage-csv#appending_to_or_overwriting_a_table_with_csv_data
    return {
        "write_if_empty" : "WRITE_EMPTY",
        "append"         : "WRITE_APPEND",
        "overwrite"      : "WRITE_TRUNCATE"
    }.get(mode)

def _data_type_mapping(python_type):
    # https://googleapis.dev/python/bigquery/latest/generated/google.cloud.bigquery.enums.StandardSqlDataTypes.html
    return {
        str   : bigquery.enums.SqlTypeNames.STRING,
        int   : bigquery.enums.SqlTypeNames.INT64,
        float : bigquery.enums.SqlTypeNames.FLOAT64
    }.get(python_type)

def _set_default_schema(columns : list or tuple) -> list:
    # define schema for the table as string
    # TODO - check type using `pd.info()` and define a schema
    return [bigquery.SchemaField(col, bigquery.enums.SqlTypeNames.STRING) for col in columns]

def upload_dataframe(
        project_name : str,
        dataset_id   : str,
        table_name   : str,
        data         : pd.DataFrame,
        **kwargs
    ) -> str:
    """Upload a pandas DataFrame to Google Big-Query"""

    # set schema, or all field are inserted as string
    schema = kwargs.get("schema", _set_default_schema(data.columns))

    # set insert type, defaults to append
    write_disposition = _write_preference(kwargs.get("write_disposition", "append"))

    # create a connection to GBQ
    client   = bigquery.Client(project = project_name)
    table_id = f"{project_name}.{dataset_id}.{table_name}"

    # define job
    job = client.load_table_from_dataframe(
            data, table_id,
            job_config = bigquery.LoadJobConfig(schema = schema, write_disposition = write_disposition)
        )

    # write to table
    job.result()

    # check number of records in table
    table = client.get_table(table_id)  # Make an API request

    return f"Total Records in {table_id} is {table.num_rows} rows with {len(table.schema)} columns."
