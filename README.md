<div align="center">

<h1 align = "center">
	Getting Started with Google Big-Query (GBQ) <br>
	<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
	<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
	<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
	<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
	<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
	<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
	<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>
</h1>

</div>

<div align="justify">

<p align = "justify"><b>TEMPLATE Design</b> built specifically for python Language, with necessary file structure as required. This TEMPLATE DOES NOT COME with a LICENSE File, but you can easily add a required license from GitHub. However, special files like .gitignore .gitattributes are included.</p>

Follow the steps to create a new license file as below:
- Open the Repository
- Click "Add file" > "Create a New File"
- Type the name of the File as: LICENSE or LICENSE.txt or LICENSE.md
- Click on "Choose a License Template"
- Review and Submit
- Commit Changes as Required

**NOTE:** (i) You can add GitHub Repository Badges from [Shields IO](https://shields.io/) - if this is a Public Repository; (ii) TAB (size = 4) has been used for indentation.

## Creating a NEW Repository from Template
<p align = "justify">Introduced in 2019, users can now create a repository from templates in GitHub. To do this, simply head over to any repository settings and enable "Template Repository" from the Options Menu. Template Repository is not limited to GitHub, and you can setup your own local-file structure for the same.</p>

```bash
# Note the use of rsync
rsync -rh ~/source/directory /destination/directory
```

## Setup Information
<p align = "justify">The template provides a <i>general</i> structure that I use (highly motivated from pandas dir and coding structure). There needs to be several things that need to be changed, just after initializing with the template, which are as follows:</p>

- `PKG` variable under `setup.py` has to be replaced with the module name and directory name `pkg-name`,
- Define High-Level Version Name (like 0.0.1, 0.1, 1.1, etc.) in `pkg-name/VERSION` file, to be interpreted by `setup.py`, and
- Define/Check/Update Author Name, Copyright Information (if any) in `setup.py` and `pkg-name/__init__.py`.

**TIP:** Install using `pip` by moving into the parent directory, with the following folder:
```bash
debmalya@machine:~$ ls -l
drwxrwxr-x 3 debmalya debmalya    4096 Aug 30 12:04  PYTHON_GIT_TEMPLATE
```
Finally, install it via:
```python
pip install ./PYTHON_GIT_TEMPLATE # Normal Installation
pip install -e ./PYTHON_GIT_TEMPLATE # Installation in Editable Mode
```

### Setting up Environment
As per the anaconda [well-known known issue](https://github.com/pypa/pip/issues/8176), I'm not able to create a `requirements.txt` file, however, `environment.yml` can be used to create a conda-environment, as follows:

```bash
conda env create -f environment.yml # creates an environment named sandbox-gbq
conda activate sandbox-gbq          # activate the specific environment

# ensure using --upgrade flag
pip install --upgrade google-cloud-storage
```

More help on [jupyter notebooks](https://gitlab.com/ZenithClown/computer-configurations-and-setups/-/tree/master/Jupyter%20Guide). You might need to change the `prefix` in `environment.yml` with the proper installation directory of your Anaconda Distribution.

**Update:** `requirements.txt` is added!

### Configuring Google Cloud SDK
Check GCP Documentations [here](https://cloud.google.com/sdk/docs/install#deb).

```bash
gcloud init
Welcome! This command will take you through the configuration of gcloud.

Your current configuration has been set to: [default]

You can skip diagnostics next time by using the following flag:
  gcloud init --skip-diagnostics

Network diagnostic detects and fixes local network connection issues.
Checking network connection...done.
Reachability Check passed.
Network diagnostic passed (1/1 checks passed).

You must log in to continue. Would you like to log in (Y/n)?  y

Go to the following link in your browser:

    https://accounts.google.com/o/oauth2/auth?response_type=code...

Enter verification code: <paste-verification-code-here>
You are logged in as: [<my-authenticated-email>]

Pick cloud project to use:
 [1] hardy-orb-188707
 [2] quixotic-folio-318907
 [3] Create a new project
Please enter numeric choice or text value (must exactly match list
item):  2

Your current project has been set to: [quixotic-folio-318907].

Do you want to configure a default Compute Region and Zone? (Y/n)?  n

Created a default .boto configuration file at [/home/pi/.boto]. See this file and
[https://cloud.google.com/storage/docs/gsutil/commands/config] for more
information about configuring Google Cloud Storage.
Your Google Cloud SDK is configured and ready to use!

* Commands that require authentication will use debmalyapramanik.005@gmail.com by default
* Commands will reference project `quixotic-folio-318907` by default
Run `gcloud help config` to learn how to change individual settings

This gcloud configuration is called [default]. You can create additional configurations if you work with multiple accounts and/or projects.
Run `gcloud topic configurations` to learn more.

Some things to try next:

* Run `gcloud --help` to see the Cloud Platform services you can interact with. And run `gcloud help COMMAND` to get help on any gcloud command.
* Run `gcloud topic --help` to learn about advanced features of the SDK like arg files and output formatting
```

## IAM Privileges
All GCP products are controlled via **IAM** Admin Panel, and each specific user level access has to be provided. To enable **`key`** for a specific person, give the user `Security Key Admin` Privilege. Finally, set the `*.json` file into **system path** under **`GOOGLE_APPLICATION_CREDENTIALS`**.

</div>
