# ==========================================
#   Copyright (c) 2020 Debmalya Pramanik   #
# ==========================================

# -------------------------------------------------------------------
#   Mnemonic:   Dockerfile
#   Abstract:   Docker File for User Guide
#               Getting Started with Google BigQuery (GBQ)
#
#               Building should be as simple as:
#                 docker build --tag gbq-getting-started:latest .
#
#               If you are using JSON Key for authenticating
#               to GBQ, then you might need to use the `-v`
#               flag to share the credentials/private key.
#               Else, you can check and/or use any other process.
#
#   Date:       5 July 2021
#   Author:     Debmalya Pramanik
# -------------------------------------------------------------------

FROM python:3.8.11

ENV DOCUMENT_ROOT /usr/src/gbq-getting-started
RUN mkdir -p $DOCUMENT_ROOT
RUN mkdir -p /usr/share/private

# install net-tools and nano-editor
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    nano \
    net-tools \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# set working directory
WORKDIR $DOCUMENT_ROOT

# install all requirements
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN python -m pip install --upgrade pip
RUN pip install --upgrade google-cloud
RUN pip install --upgrade google-cloud-storage
RUN pip install --upgrade google-cloud-bigquery
RUN pip install --upgrade googleapis-common-protos

# update repository
RUN apt-get update

# copy all files and folder to docker
COPY . .

# run the application in docker environment
CMD [ "python", "./main.py" ]
