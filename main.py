# -*- encoding: utf-8 -*-

import os
import numpy as np
import pandas as pd

from gbq import *

# generate a 25x3 dummy dataframe
create_dummy_data = lambda : pd.DataFrame(np.random.random(size = (25, 3)), columns = "A B C".split())

def main(dataframe : pd.DataFrame):
    # set configuration for google cloud project
    GCP_PROJECT   = "quixotic-folio-318907" # should be same as the Project-ID
    GBQ_DATASET   = "pandas_demo"           # create a dataset
    BQ_TABLE_NAME = "test"                  # the table is automatically created - if not exists

    # define the data schema
    schema = [bigquery.SchemaField(col, bigquery.enums.SqlTypeNames.FLOAT64) for col in dataframe.columns]

    return upload_dataframe(
        project_name = GCP_PROJECT,
        dataset_id   = GBQ_DATASET,
        table_name   = BQ_TABLE_NAME,
        data         = dataframe,

        # since dummy data, overwrite the data
        schema            = schema,
        write_disposition = "overwrite"
    )

if __name__ == '__main__':
    # if running from a docker container, then the credential file is copied
    # into the docker environment, and is set as an argument using `os.environ`
    # uncomment the next line to run from docker
    # else the key file should be properly mapped into system path
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/", "usr", "share", "private", "quixotic-folio-318907-64bfdccfb050.json")
    dataframe = create_dummy_data()
    print(main(dataframe)) # execute and post data
